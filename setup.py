from setuptools import find_package, setup

setup(
    name='Super Web API',
    version='0.4',
    packages=find_packages(),
    inclide_data_package=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
)
