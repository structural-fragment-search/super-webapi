[![pipeline status](https://gitlab.com/structural-fragment-search/super-webapi/badges/master/pipeline.svg)](https://gitlab.com/structural-fragment-search/super-webapi/commits/master)

[![coverage report](https://gitlab.com/structural-fragment-search/super-webapi/badges/master/coverage.svg)](https://structural-fragment-search.gitlab.io/super-webapi/)

## Super WebAPI
## Query
- /api/super/search POST
Returns 202 Accepted
Location: /api/super/queue/`<identifier>`

## Job queue
- /api/queue GET
Queue status including number of queued jobs
- /api/super/queue/`<identifier>` GET
Job status including rank, whether it is running, ETA, and progress.
Once the search completes, return 303 See other with Location: /api/super/result/`<identifier>`
- /api/super/queue/`<identifier>` DELETE
Cancel a job

## Results
- /api/super/result/`<identifier>` GET
Search results
