import uuid
import datetime

from flask import request
from flask_restplus import Namespace, fields, Resource
from superwebapi import db
from superwebapi.job import Job


def create_new_job(expression, data):
    new_job = Job(
        job_id=str(uuid.uuid4()),
        submitted=datetime.datetime.utcnow(),
        query_expression=expression,
        query_data=data
    )
    save_changes(new_job)
    response_object = {
        'Status': 'success',
        'Location': new_job.job_id
    }
    return response_object, 202

def get_all_jobs():
    return Job.query.filter_by(queued=True).all()

def get_job(job_id):
    return Job.query.filter_by(job_id=job_id).first()

def pop_queued_job():
    q_job = Job.query.filter_by(queued=True).order_by(Job.submitted).first()
    if q_job is not None:
        q_job.queued = False
        q_job.running = True
        q_job.started = datetime.datetime.utcnow()
        db.session.commit()
    return q_job

def save_changes(data):
    db.session.add(data)
    db.session.commit()

    
api = Namespace('queue', description='Job queue related operations')
_job = api.model('queue', {
    'job_id': fields.String(description='Job identifier'),
    'submitted': fields.String(description='Job submission time'),
    'queued': fields.String(description='Job is queued'),
    'running': fields.String(description='Job is running'),
    'query_expression': fields.String(description='Query expression', required=True),
    'query_data': fields.String(description='Query data', required=True),
})

@api.route('/')
class JobList(Resource):
    @api.doc('list_of_queued_jobs')
    @api.marshal_list_with(_job, envelope='queue')
    def get(self):
        """ List all queued jobs """
        return get_all_jobs()

    @api.response(202, 'Job successfully queued')
    @api.doc('Create a new job')
    @api.expect(_job, validate=True)
    def post(self):
        """ Creates a new job """
        query_expression = request.json['query_expression']
        query_data = request.json['query_data']
        return create_new_job(query_expression, query_data)

@api.route('/<job_id>')
@api.param('job_id', 'A job identifier')
@api.response(404, 'Job does not exist')
class JobItem(Resource):
    @api.doc('Get a job')
    @api.marshal_with(_job)
    def get(self, job_id):
        """ Get a Job given its identifier """
        my_job = get_job(job_id)
        if not my_job:
            api.abort(404)
        else:
            return my_job

@api.route('/pop')
class JobQueue(Resource):
    @api.doc('Get the job that will next be run')
    @api.marshal_with(_job)
    def get(self):
        """ Get the job that will be run next """
        my_job = pop_queued_job()
        if not my_job:
            api.abort(404)
        else:
            return my_job
