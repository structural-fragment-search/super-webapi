from . import celery
from superwebapi.queue import pop_queued_job

@celery.task
def add(x, y):
    return x + y

@celery.task
def run_search(expression, data):
    job = pop_queued_job()
    if job is not None:
        # Run super on the job
        # results = run_super(expression, data)
        # for result in results: insert_result(result)
        pass
