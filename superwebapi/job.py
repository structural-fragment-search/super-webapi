from . import db

class Job(db.Model):
    """ Model for storing job related details """
    __tablename__ = "job"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    job_id = db.Column(db.String(100), unique=True)
    submitted = db.Column(db.DateTime, nullable=False)
    started = db.Column(db.DateTime, nullable=True)
    queued = db.Column(db.Boolean, nullable=False, default=True)
    running = db.Column(db.Boolean, nullable=False, default=False)
    query_expression = db.Column(db.Text, nullable=True)
    query_data = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return f"<Job {self.id}: queued({self.queued}) running({self.running})>"
