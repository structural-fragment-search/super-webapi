import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    enable_utc = True
    SECRET_KEY = os.getenv('SECRET_KEY', 'dev')
    DEBUG = False

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'super.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_RESULT_BACKEND = 'redis://localhost'
    CELERY_BROKER = 'redis://localhost'

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'super.db')
    SQLALCHEMY_TRACK_MODIFICATION = False
    PRESERVE_CONTEXT_ON_EXCEPTION = True
    CELERY_RESULT_BACKEND = 'redis://localhost'
    CELERY_BROKER = 'redis://localhost'

class ProductionConfig(Config):
    DEBUG = False

config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
