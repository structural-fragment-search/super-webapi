from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api
from celery import Celery

from .config import config_by_name

db = SQLAlchemy()

celery = Celery('superwebapi', backend='redis://localhost', broker='redis://localhost')

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    celery.conf.update(app.config)

    class ContextClass(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)
    celery.Task = ContextClass
    
    return app

from .queue import api as queue_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Super web API',
          version='0.1',
          description='A service for running super opigopeptide searches'
)
api.add_namespace(queue_ns, path='/queue')
