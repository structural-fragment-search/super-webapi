import unittest
import datetime

from superwebapi import db
from superwebapi.job import Job
from superwebapi.queue import create_new_job, get_all_jobs
from test.base import BaseTestCase

class TestJobQueue(BaseTestCase):
    def test_fetch_queued_jobs(self):
        create_new_job("\\1", "{data}")
        queued_jobs = get_all_jobs()
        self.assertTrue(len(queued_jobs) == 1)
        self.assertTrue(queued_jobs[0].queued is True)

    def test_empty_queue(self):
        queued_jobs = get_all_jobs()
        self.assertTrue(len(queued_jobs) == 0)

    def test_no_queued_items(self):
        new_job = Job(
            job_id = "test",
            submitted = datetime.datetime.utcnow(),
            queued = False
        )
        db.session.add(new_job)
        db.session.commit()

        queued_jobs = get_all_jobs()
        self.assertTrue(len(queued_jobs) == 0)

if __name__ == "__main__":
    unittest.main()
